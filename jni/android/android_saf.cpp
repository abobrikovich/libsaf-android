#include "android/saf.h"

#include <unistd.h>
#include <libgen.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"

static JavaVM *jvm;
static jobject jcontext;
static jclass jsaf_helper;
static jmethodID jsaf_open;

static FILE* _safopen(const char *fname, const char *fmode) {
	GET_JENV(env, jvm);

	jstring path = env->NewStringUTF(fname);
	jstring mode = env->NewStringUTF(fmode);

	int fd = env->CallStaticIntMethod(jsaf_helper, jsaf_open, path, mode);

	LOGD("path=%p mode=%p fd=%d", path, mode, fd);

	env->DeleteLocalRef(path);
	env->DeleteLocalRef(mode);
	RELEASE_JENV(env, jvm);

	return (fd > 0) ? fdopen(fd, fmode) : 0;
}

void safinit(JNIEnv *env, jobject context) {
	jint rs = env->GetJavaVM(&jvm);

	jcontext = context;
	jsaf_helper = env->FindClass("org/camobap/ndk/saf/SAFHelper");

	jsaf_open = env->GetStaticMethodID(jsaf_helper, "openFD", "(Ljava/lang/String;Ljava/lang/String;)I");

	jmethodID jsaf_init = env->GetStaticMethodID(jsaf_helper, "init", "(Landroid/content/Context;)V");
	env->CallStaticVoidMethod(jsaf_helper, jsaf_init, context);
}

FILE	*safopen(const char * __restrict fname, const char * __restrict fmode) {
	LOGD("fname=%s fmode=%s errno=%d", fname, fmode, errno);
	// check unix permissions for dir
	char *dname = dirname(strdup(fname));
	access(dname, R_OK | W_OK);

	LOGD("dname=%s errno=%d", dname, errno);

	FILE *file = nullptr;
	if (strstr(dname, "content://com.android.externalstorage.documents")) {
		file = _safopen(fname, fmode);
	} else {
		// try to open posix file
		file = fopen(fname, fmode);
		if (!file && errno == EACCES) {	// our job to fix this
			LOGD("file=%p fname=%s errno=%d", file, fname, errno);
			file = _safopen(fname, fmode);
		}
	}

	return file;
}
