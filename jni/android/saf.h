/*
 * saf.h
 *
 *  Created on: Oct 21, 2015
 *      Author: camobap
 */

#pragma once

#include <jni.h>
#include <stdio.h>

void	safinit(JNIEnv *env, jobject context);

FILE	*safopen(const char * __restrict fname, const char * __restrict mode);

