#pragma once

#include <android/log.h>

// http://stackoverflow.com/questions/19343205/c-concatenating-file-and-line-macros
#define STRINGIFY(x) #x
#define STRINGIFY2(x) STRINGIFY(x)

#ifndef LOG_TAG
#define LOG_TAG    __FILE__ ":" STRINGIFY2(__LINE__)
#endif

#ifdef DISABLE_LOGCAT

#define LOGI(...)  ((void)0)
#define LOGW(...)  ((void)0)
#define LOGE(...)  ((void)0)
#define LOGD(...)  ((void)0)

#else

#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

#endif

#define ASM_CMT(str) asm volatile("@ " str)

#define assert0(cond, msg) do { \
if(!(cond)) { \
	__android_log_assert(STRINGIFY(cond), LOG_TAG, "assert(%s) -> %s():%d %s", STRINGIFY(cond), __func__, __LINE__, msg); \
} \
} while (0) \

#if defined(ASSERT_LEVEL) && ASSERT_LEVEL > 0
#define assert1(cond) assert0(cond, "")
#define assert2(cond, msg) assert0(cond, msg)
#else
#define assert1(cond) ((void)0)
#define assert2(cond, msg) ((void)0)
#endif

#define assert_unreachable(msg)	assert2(0, "assert_unreachable: " msg)

#define GET_JENV(env, jvm) \
        JNIEnv* env = nullptr; \
        bool alreadyAttached = (jvm->GetEnv((void**)&env, JNI_VERSION_1_6) == JNI_OK); \
        if (!alreadyAttached) { \
            jvm->AttachCurrentThread(&env, 0); \
        }

#define RELEASE_JENV(env, jvm) \
        if (!alreadyAttached) { \
            jvm->DetachCurrentThread(); \
        }

