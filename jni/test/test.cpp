/*
 * test.cpp
 *
 *  Created on: Nov 13, 2015
 *      Author: camobap
 */

#include "org_camobap_ndk_saf_test_TestActivity.h"

#include <android/saf.h>
#include <string.h>

JNIEXPORT void JNICALL Java_org_camobap_ndk_saf_test_TestActivity_test
  (JNIEnv *env, jobject thiz, jstring dir) {

	safinit(env, thiz);

	char temp[256];
	const char *dir_c = env->GetStringUTFChars(dir, 0);

	memset(temp, 0, 256);
	strcpy(temp, dir_c);
	strcat(temp, "/text.txt");

	FILE *f = safopen(temp, "w");
	if (f) {
		fwrite("test", 1, 5, f);
		fclose(f);
	}

	memset(temp, 0, 256);
	strcpy(temp, dir_c);
	strcat(temp, "/text2.txt");

	FILE *f2 = safopen(temp, "w");
	if (f2) {
		fwrite("test2", 1, 6, f2);
		fclose(f2);
	}

	env->ReleaseStringUTFChars(dir, dir_c);
}
