LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    				:= saf_android
LOCAL_SRC_FILES 				:= android/android_saf.cpp
LOCAL_CFLAGS 					:= -std=gnu++11
LOCAL_LDFLAGS 					:= -llog

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES			:= saf_android
LOCAL_MODULE    				:= saf_android_test
LOCAL_C_INCLUDE 				:= .
LOCAL_SRC_FILES 				:= test/test.cpp

include $(BUILD_SHARED_LIBRARY)