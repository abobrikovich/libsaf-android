package org.camobap.ndk.saf.test;

import org.camobap.ndk.saf.R;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.support.v4.provider.DocumentFile;
import android.util.Log;
import android.view.View;

/**
 * javah -v -jni -classpath src -d jni/test org.camobap.ndk.saf.test.TestActivity
 * @author camobap
 */
public class TestActivity extends Activity {

	private static final int SAF_DIR_REQUEST_CODE = 123;

	static {
		System.loadLibrary("saf_android");
		System.loadLibrary("saf_android_test");
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.test_activity);
	}
	
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public void testClick(View view) {

	    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
	    startActivityForResult(intent, SAF_DIR_REQUEST_CODE);
	}
	
	@TargetApi(Build.VERSION_CODES.KITKAT)
	@Override 
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    switch (requestCode) {
	        case SAF_DIR_REQUEST_CODE:   
	            if (resultCode == RESULT_OK) {
	            	final Uri uri = data.getData();
	            	final String uriString = Uri.decode(uri.toString());
	            	String path = null;
	            	if (uriString.contains("/tree/primary:")) {	// skip ASF
	            		path = uri.getPath().replace("/tree/primary:", "");
	            		path = Environment.getExternalStorageDirectory().getAbsolutePath() + path;
	            		Log.d("SAF", "Local path");
	            	} else {
		        		this.getContentResolver().takePersistableUriPermission(uri, 
		        	            Intent.FLAG_GRANT_READ_URI_PERMISSION |
		        	            Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
		                
		        		Uri test = DocumentFile.fromTreeUri(this, uri).getUri();
		        		
		                // Get the File path from the Uri
		                path = test.toString();//getNameFromURI(uri);
		
	            	}
                	test(path);
	            }
	            break;
	    }
	} 
	
	private String getNameFromURI(Uri contenturi){
		 
	    String[] proj = {
	            OpenableColumns.DISPLAY_NAME,
	            OpenableColumns.SIZE
	    }; 
	    String name = null;
	    int size= 0;
	    Cursor metadataCursor = getContentResolver().query(contenturi,  proj, null, null, null);
	 
	    if (metadataCursor != null) {
	        try { 
	            if (metadataCursor.moveToFirst()) {
	                name = metadataCursor.getString(0);
	                size = metadataCursor.getInt(1);
	            } 
	        } finally { 
	            metadataCursor.close();
	        } 
	    } 
	 
	    return name;
	} 
	
	private native void test(String dir);
}
