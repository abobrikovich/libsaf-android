package org.camobap.ndk.saf;

import java.io.File;
import java.io.FileNotFoundException;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.UriPermission;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.provider.DocumentFile;

public class SAFHelper {
	private static Context mContext;
	
	public static final int NO_EXIST_CODE = -1;
	public static final int NO_ACCES_CODE = -2;
	
	public static void init(final Context context) {
		mContext = context;
	}
	
	public static void requestSDCardPermissions() {
		String path = Environment.getExternalStorageDirectory().getPath();
		DocumentFile rootSDCard = DocumentFile.fromFile(new File(path));
		mContext.grantUriPermission("org.camobap.ndk.saf", 
				rootSDCard.getUri(), Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
	}
	
	@TargetApi(Build.VERSION_CODES.KITKAT)
	public static int openFD(final String path, final String mode) {
		String dir = path.substring(0, path.lastIndexOf('/') - 1);
		String file = path.substring(path.lastIndexOf('/') + 1);
		DocumentFile docDir = DocumentFile.fromTreeUri(mContext, Uri.parse(dir));
		
		boolean writeAccess = false;
		for (UriPermission up : mContext.getContentResolver().getPersistedUriPermissions()) {
			if (docDir.getUri().toString().contains(up.getUri().toString())) {
				writeAccess = up.isWritePermission();
			}
		}
		
		if (writeAccess) {
		
			DocumentFile docFile = docDir.createFile("video/H264", file);
	
			try {
				return mContext.getContentResolver().openFileDescriptor(docFile.getUri(), mode).detachFd();
			} catch (FileNotFoundException e) {
				return NO_EXIST_CODE;
			}
		} else {
			return NO_ACCES_CODE;
		}
	}
}
